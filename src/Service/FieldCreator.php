<?php

namespace Drupal\actitoolbox\Service;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Class FieldCreator.
 */
class FieldCreator {

  /**
   * Create the new field for the conversion.
   *
   * @param array $values
   * @param string $type
   * @return boolean
   */
  public function createField(array $values, string $type): bool
  {
    // Creating new field.
    if ($values['new_field_cardinality_limited'] == 'unlimited') {
      $cardinality = -1;
    }
    else {
      $cardinality = $values['new_field_cardinality'];
    }

    try {
      FieldStorageConfig::create([
        'field_name' => 'field_' . $values['field_machine_name'],
        'entity_type' => 'node',
        'type' => $type,
        'cardinality' => $cardinality,
      ])->save();
  
      FieldConfig::create([
        'field_name' => 'field_' . $values['field_machine_name'],
        'entity_type' => 'node',
        'bundle' => $values['content_type'],
        'label' => $values['field_label'],
      ])->save();
    } catch (\Throwable $th) {
      \Drupal::logger('field_convert')->error($th->getMessage());
      return FALSE;
    }
    return TRUE;
  }

}

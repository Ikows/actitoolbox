<?php

namespace Drupal\actitoolbox\Form;

use Drupal\actitoolbox\Form\ConvertForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ListStringToBoolean.
 */
class ListStringToBoolean extends ConvertForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_string_to_boolean';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $allowedValues = $this->fieldConfig->getFieldStorageDefinition()->getSettings()["allowed_values"];

    $form['correspondance'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Data conversion'),
    ];

    $counter = 0;
    $valuesToSubmit = [];
    foreach ($allowedValues as $optionValue => $optionLabel) {
      $valuesToSubmit[] = $optionValue;
      $form['correspondance']['list_string_' . $counter] = [
        '#type' => 'radios',
        '#title' => $optionLabel . ' (' . $optionValue . ')',
        '#options' => [0 => $this->t('Inactive'), 1 => $this->t('Active')],
        '#required' => TRUE,
      ];
      $counter++;
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    $form_state->set('values_to_submit', $valuesToSubmit);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $valuesToSubmit = $form_state->get('values_to_submit');

    $conversionArray = [];
    foreach ($values as $fieldName => $boolValue) {
      if (strpos($fieldName, 'list_string_') !== FALSE) {
        $key = explode('_', $fieldName)[2];
        $conversionArray += [
          $key => [
            'old_val' => $valuesToSubmit[$key],
            'new_val' => $boolValue,
          ]
        ];
      }
    };

    // Creating the new field.
    if ($this->fieldCreator->createField($values, 'boolean')) {
      // Load nodes of selected bundle.
      $nids = \Drupal::entityQuery('node')->condition('type',$values['content_type'])->execute();
      if ($nids) {
        $batch = $this->prepareBatch($nids, $values, $conversionArray, 'list_string_to_boolean');
        batch_set($batch);
      }
    }
    else {
      \Drupal::messenger()->addError($this->t('An error has occured during the creation of the new field'));
    }

  }

}
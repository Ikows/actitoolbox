<?php

namespace Drupal\actitoolbox\Form;

use Drupal\actitoolbox\Form\ConvertForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EmailToEmail.
 */
class EmailToEmail extends ConvertForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_to_email';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Creating the new field.
    if ($this->fieldCreator->createField($values, 'email')) {
      // Load nodes of selected bundle.
      $nids = \Drupal::entityQuery('node')->condition('type',$values['content_type'])->execute();
      if ($nids) {
        $batch = $this->prepareBatch($nids, $values, [], 'email_to_email');
        batch_set($batch);
      }
    }
    else {
      \Drupal::messenger()->addError($this->t('An error has occured during the creation of the new field'));
    }
  }

}

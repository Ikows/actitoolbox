<?php

namespace Drupal\actitoolbox\Form;

use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormBase;
use Drupal\node\Entity\NodeType;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class FieldReplaceForm.
 */
class FieldReplaceForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_replace_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'actitoolbox/field_replace_form';

    $form['description'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $this->t('Here you can choose a field from a content type') . '</p><p>' . $this->t('Then the form will show you the field types that are compatibles to perform a replacement') . '</p>',
    ];

    // Content type select field.
    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $this->getContentTypes(),
      '#description' => $this->t('Select the content type thats contains the field you want to replace'),
      '#size' => 0,
      '#ajax' => [
        'callback' => '::findFields',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'field_to_replace_container',
      ],
      '#required' => TRUE,
    ];

    // Field to replace field.
    $form['field_to_replace_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['field_to_replace_container'],
      ],
    ];

    $selectedContentType = $form_state->getValue('content_type');
    if (!empty($selectedContentType)) {
      $form['field_to_replace_container']['field_to_replace'] = [
        '#type' => 'select',
        '#title' => $this->t('Field to replace'),
        '#options' => $this->getContentTypeFields($selectedContentType),
        '#description' => $this->t('Select the field to replace (machine name)'),
        '#required' => TRUE,
        '#ajax' => [
          'callback' => '::findFieldType',
          'disable-refocus' => FALSE,
          'event' => 'change',
          'wrapper' => 'field_type_to_replace_container',
        ],
      ];
    }

    // Field type html tag and available types field container.
    $form['field_to_replace_container']['field_type_to_replace_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['field_type_to_replace_container'],
      ],
    ];

    if ($form_state->hasValue('field_to_replace') &&  $form_state->hasValue('content_type')) {
      $selectedContentTypeField = $form_state->getValue('field_to_replace');
      if (!empty($selectedContentType) && !empty($selectedContentTypeField)) {
        // Html field for field type infos.
        $form['field_to_replace_container']['field_type_to_replace_container']['field_infos_fs'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Field infos'),
        ];

        $form['field_to_replace_container']['field_type_to_replace_container']['field_infos_fs']['field_infos'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->getFieldInfos($selectedContentType, $selectedContentTypeField, 'html'),
        ];

        $form['field_to_replace_container']['field_type_to_replace_container']['hidden_type'] = [
          '#type' => 'hidden',
          '#value' => $this->getFieldInfos($selectedContentType, $selectedContentTypeField, 'type'),
        ];

        // Select field for available field types that can be used for the new field.
        $form['field_to_replace_container']['field_type_to_replace_container']['available_field_types'] = [
          '#type' => 'select',
          '#title' => $this->t('Available field types'),
          '#description' => $this->t('Field types that can be used for the new field'),
          '#options' => $this->getAvailableFieldTypes($this->getFieldInfos($selectedContentType, $selectedContentTypeField, 'type')),
          '#required' => TRUE,
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Configure the replacement process'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    \Drupal::request()->getSession()->set('first_form_values', $values);
    $formType = $values['hidden_type'] . '_to_' . $values['available_field_types'];

    //TODO make dispatcher.

    \Drupal::messenger()->addMessage($formType);
    $form_state->setRedirect('actitoolbox.' . $formType);
  }

  /**
   * Ajax process for rendering [field_to_replace_container].
   *
   * @param array $form
   *   The form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The field [field_to_replace_container].
   */
  public function findFields(array &$form, FormStateInterface $form_state) :array {
    return $form['field_to_replace_container'];
  }

  /**
   * Ajax process for rendering [field_type_to_replace_container].
   *
   * @param array $form
   *   The form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The field [field_type_to_replace_container].
   */
  public function findFieldType(array &$form, FormStateInterface $form_state) :array {
    return $form['field_to_replace_container']['field_type_to_replace_container'];
  }

  /**
   * Loading Content types.
   *
   * @return array
   *   Containing content types id => label or empty array.
   */
  protected function getContentTypes() :array {
    $options = [];
    if ($nodeTypes = NodeType::loadMultiple()) {
      foreach ($nodeTypes as $nodeType) {
        $options[$nodeType->id()] = $nodeType->label();
      }
    }
    return $options;
  }

  /**
   * Loading Content type custom fields.
   *
   * @param string $selectedContentType
   *   The selected node bundle.
   *
   * @return array
   *   Containing content type fields id => id or empty array.
   */
  protected function getContentTypeFields(string $selectedContentType) :array {
    $fields_options = [];
    if ($selectedContentType) {
      if ($node = Node::create(['type' => $selectedContentType])) {
        if ($fields = $node->getFieldDefinitions()) {
          foreach ($fields as $field_id => $field) {
            if (strpos($field_id, 'field_') !== FALSE) {
              $fields_options[$field_id] = $field_id;
            }
          }
        }
      }
    }
    return $fields_options;
  }

  /**
   * Load some infos on the selected field.
   *
   * @param string $bundle
   *   The selected node bundle.
   * @param string $fieldName
   *   The selected field name.
   * @param string $mode
   *   Mode "html" for field type infos or "type" for just the type.
   *
   * @return string
   *   Html string to to be rendered in html Drupal form element.
   */
  protected function getFieldInfos(string $bundle, string $fieldName, string $mode) :string {
    $fieldConfig = FieldConfig::loadByName('node', $bundle, $fieldName);
    $resp = 'error';
    switch ($mode) {
      case 'html':
        /* dump($fieldConfig->getFieldStorageDefinition()->getCardinality()); */
        $cardinality = $fieldConfig->getFieldStorageDefinition()->getCardinality();
        if ($cardinality == -1) {
          $cardinality = '-1 (' . t('Unlimited') . ')';
        }
        $html = '';
        $html .= '<p>' . $this->t('Field type') . ' : <strong>' . $this->typeToString($fieldConfig->getType()) . '</strong></p>';
        $html .= '<p>' . $this->t('Format des données') . ' : <strong>' . $fieldConfig->getType() . '</strong></p>';
        $html .= '<p>' . $this->t('Allowed number of values') . ' : <strong>' . $cardinality . '</strong></p>';
        $resp = $html;
        break;

      case 'type':
        $resp = $fieldConfig->getType();
        break;
    }
    return $resp;
  }

  /**
   * Transform field type into Translatable markup.
   *
   * @param string $type
   *   Field type.
   *
   * @return Drupal\Core\StringTranslation\TranslatableMarkup
   *   Markup corresponding to field type.
   */
  protected function typeToString(string $type) :TranslatableMarkup { //TODO : convert to service
    $equiArray = [
      'string' => $this->t('Text (plain)'),
      'string_long' => $this->t('Text (plain, long)'),
      'text' => $this->t('Text (formatted)'),
      'text_long' => $this->t('Text (formatted, long)'),
      'text_with_summary' => $this->t('Text (formatted, long, with summary)'),
      'boolean' => $this->t('Boolean'),
      'list_string' => $this->t('List (text)'),
      'entity_reference' => $this->t('Entity reference'),
      'email' => $this->t('Email'),
    ];
    if (isset($equiArray[$type])) {
      return $equiArray[$type];
    }
    else {
      return $this->t('');
    }
  }

  /**
   * Get the availables field types matching with a conversion.
   *
   * @param string $fieldType
   *   Machine name of the field type from the field to convert.
   *
   * @return array
   *   Array of options.
   */
  protected function getAvailableFieldTypes(string $fieldType) :array {
    $options = [];
    switch ($fieldType) {
      case 'string':
        $options[$this->t('Text')->render()] = [];
        $availables = [
          'string',
          //'string_long',
          //'text',
          //'text_long',
          //'text_with_summary',
        ];
        $this->generateOptions($availables, $options[$this->t('Text')->render()]);
        break;

/*       case 'string_long':
        $options[$this->t('Text')->render()] = [];
        $availables = [
          'string_long',
          'text_long',
          'text_with_summary',
        ];
        $this->generateOptions($availables, $options[$this->t('Text')->render()]);
        break; */

/*       case 'text':
        $options[$this->t('Text')->render()] = [];
        $availables = [
          'text',
          'text_long',
          'text_with_summary',
        ];
        $this->generateOptions($availables, $options[$this->t('Text')->render()]);
        break; */

/*       case 'text_long':
      case 'text_with_summary':
        $options[$this->t('Text')->render()] = [];
        $availables = [
          'text_long',
          'text_with_summary',
        ];
        $this->generateOptions($availables, $options[$this->t('Text')->render()]);
        break; */

      case 'boolean':
        $options[$this->t('General')->render()] = [];
        $generalAvailables = [
          'boolean',
        ];
        $this->generateOptions($generalAvailables, $options[$this->t('General')->render()]);
/*         $options[$this->t('Text')->render()] = [];
        $textAvailables = [
          'string',
          'string_long',
          'text',
          'text_long',
          'text_with_summary',
          'list_string',
        ];
        $this->generateOptions($textAvailables, $options[$this->t('Text')->render()]); */
        break;

      case 'entity_reference':
      case 'list_string':
        $options[$this->t('General')->render()] = [];
        $generalAvailables = [
          'boolean',
        ];
        $this->generateOptions($generalAvailables, $options[$this->t('General')->render()]);
/*         $options[$this->t('Text')->render()] = [];
        $textAvailables = [
          'list_string',
        ];
        $this->generateOptions($textAvailables, $options[$this->t('Text')->render()]); */
        break;
      
      case 'email':
        $options[$this->t('General')->render()] = [];
        $generalAvailables = [
          'email',
        ];
        $this->generateOptions($generalAvailables, $options[$this->t('General')->render()]);
/*         $options[$this->t('Text')->render()] = [];
        $availables = [
          'string',
          //'string_long',
          //'text',
          //'text_long',
          //'text_with_summary',
        ];
        $this->generateOptions($availables, $options[$this->t('Text')->render()]); */
        break;

      default:
        break;
    }
    return $options;
  }

  /**
   * Generates an options array from field types group.
   *
   * @param array $availables
   *   Array of string containing available field types machine names.
   * @param array $optgroup
   *   Translated markup for the category.
   *
   * @return array
   *   Array of options with corresponding optiongroup.
   */
  protected function generateOptions(array $availables, array &$optgroup) :array {
    foreach ($availables as $key => $value) {
      $optgroup += [$value => $this->typeToString($value)];
    }
    return $optgroup;
  }

}

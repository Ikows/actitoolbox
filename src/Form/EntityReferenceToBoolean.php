<?php

namespace Drupal\actitoolbox\Form;

use Drupal\node\Entity\Node;
use Drupal\field\Entity\FieldConfig;
use Drupal\actitoolbox\Form\ConvertForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Class EntityReferenceToBoolean.
 */
class EntityReferenceToBoolean extends ConvertForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_reference_to_boolean';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $terms = [];
    foreach ($this->fieldConfig->getSettings()["handler_settings"]["target_bundles"] as $key => $vid) {
      $terms = array_merge($terms, \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid));
    }

    $form['correspondance'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Data conversion'),
    ];

    foreach ($terms as $key => $term) {
      $form['correspondance']['entity_ref_' . $term->tid] = [
        '#type' => 'radios',
        '#title' => '[' . $term->vid . '] ' . $term->name . ' (' . $term->tid . ')',
        '#options' => [0 => $this->t('Inactive'), 1 => $this->t('Active')],
        '#required' => TRUE,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $conversionArray = [];
    foreach ($values as $key => $value) {
      if (strpos($key, 'entity_ref_') !== FALSE) {
        $tid = explode('_', $key)[2];
        $conversionArray += [$tid => $value];
      }
    };

    // Creating the new field.
    if ($this->fieldCreator->createField($values, 'boolean')) {
      // Load nodes of selected bundle.
      $nids = \Drupal::entityQuery('node')->condition('type',$values['content_type'])->execute();
      if ($nids) {
        $batch = $this->prepareBatch($nids, $values, $conversionArray, 'entity_reference_to_boolean');
        batch_set($batch);
      }
    }
    else {
      \Drupal::messenger()->addError($this->t('An error has occured during the creation of the new field'));
    }
  }

}
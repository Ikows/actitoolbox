<?php

namespace Drupal\actitoolbox\Form;

use Drupal\Core\Form\FormBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\actitoolbox\Service\FieldCreator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConvertForm.
 */
class ConvertForm extends FormBase {

  protected $first_form_values;
  protected $fieldConfig;
  protected $fieldCreator;

  /**
   * Class constructor.
   *
   * @param FieldCreator $fieldCreator
   */
  public function __construct(FieldCreator $fieldCreator)
  {
    $this->first_form_values = \Drupal::request()->getSession()->get('first_form_values');
    $this->fieldConfig = FieldConfig::loadByName('node', $this->first_form_values['content_type'], $this->first_form_values['field_to_replace']);
    $this->fieldCreator = $fieldCreator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('field_convert.field_creator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'convert_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'actitoolbox/field_replace_form';

    $form['recap_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'recap',
    ];

    $html = '<p>' . $this->t('Content type') . ' : <strong>' . $this->first_form_values['content_type'] . '</strong></p>';
    $html .= '<p>' . $this->t('Old Field to convert') . ' : <strong>' . $this->first_form_values['field_to_replace'] . '</strong></p>';
    $html .= '<p>' . $this->t('Old field cardinality') . ' : <strong>' . $this->fieldConfig->getFieldStorageDefinition()->getCardinality() . '</strong></p>';
    $html .= '<p>' . $this->t('New field type') . ' : <strong>' . $this->first_form_values['available_field_types'] . '</strong></p>';

    $form['field_to_replace'] = [
      '#type' => 'hidden',
      '#value' => $this->first_form_values['field_to_replace'],
    ];

    $form['content_type'] = [
      '#type' => 'hidden',
      '#value' => $this->first_form_values['content_type'],
    ];

    $form['recap_fieldset']['recap'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $html,
    ];

    $form['new_field_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('New field configuration'),
    ];

    $form['new_field_config']['field_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#required' => TRUE,
    ];

    $form['new_field_config']['auto_machine_name'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '<span>' . $this->t('Machine name') . ' : field_<span id="amnVal"></span></span> [<span class="link" type ="button">' . $this->t('Edit') . '</span>]',
    ];

    $form['new_field_config']['field_machine_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Machine name'),
      '#required' => TRUE,
      '#maxlength' => 26,
    ];

    $form['new_field_config']['new_field_cardinality_limited'] = [
      '#type' => 'select',
      '#default_value' => 'limited',
      '#title' => $this->t('Allowed number of values'),
      '#options' => [
        'limited' => $this->t('Limited'),
        'unlimited' => $this->t('Unlimited'),
      ],
      '#required' => TRUE,
    ];

    $form['new_field_config']['new_field_cardinality'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 99,
      '#default_value' => 1,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Machine name unicity validation.
    if (FieldStorageConfig::loadByName('node', 'field_' . $values['field_machine_name'])) {
      $form_state->setErrorByName('field_machine_name', $this->t('This machine name already exists'));
    }

    // Machine name format validation.
    $pattern = '/[^a-z0-9_]+/';
    if (preg_match($pattern, $values['field_machine_name'])) {
      $form_state->setErrorByName('field_machine_name', $this->t('The machine name is not well formatted, use only Alphanumeric and _'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  protected function prepareBatch($nids, $values, $conversionArray, $type)
  {
    $batch = [
      'title' => t('Exporting'),
      'operations' => [
        [
          'convertField',
          [
            $nids,
            $values['field_to_replace'],
            $values['field_machine_name'],
            $conversionArray,
            $type,
          ],
        ],
      ],
      'finished' => 'convertFieldCallback',
    ];
    return $batch;
  }

}

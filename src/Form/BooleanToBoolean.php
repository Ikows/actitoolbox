<?php

namespace Drupal\actitoolbox\Form;

use Drupal\actitoolbox\Form\ConvertForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BooleanToBoolean.
 */
class BooleanToBoolean extends ConvertForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'boolean_to_boolean';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $boolValues = $this->fieldConfig->getSettings();

    $form['correspondance'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Data conversion'),
    ];

    $form['correspondance']['boolean_true'] = [
      '#type' => 'radios',
      '#title' => $boolValues['on_label'] . ' (' . $this->t('On') . ')',
      '#options' => [0 => $this->t('Off'), 1 => $this->t('On')],
      '#required' => TRUE,
    ];

    $form['correspondance']['boolean_false'] = [
      '#type' => 'radios',
      '#title' => $boolValues['off_label'] . ' (' . $this->t('Off') . ')',
      '#options' => [0 => $this->t('Off'), 1 => $this->t('On')],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $conversionArray = [
      0 => $values['boolean_false'],
      1 => $values['boolean_true'],
    ];

    // Creating the new field.
    if ($this->fieldCreator->createField($values, 'boolean')) {
      // Load nodes of selected bundle.
      $nids = \Drupal::entityQuery('node')->condition('type',$values['content_type'])->execute();
      if ($nids) {
        $batch = $this->prepareBatch($nids, $values, $conversionArray, 'boolean_to_boolean');
        batch_set($batch);
      }
    }
    else {
      \Drupal::messenger()->addError($this->t('An error has occured during the creation of the new field'));
    }

  }

}
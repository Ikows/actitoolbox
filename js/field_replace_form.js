/**
 * @file
 * Contains the definition of the behaviour contentTypeReset, autoMachineName and hideCardinalityNumber.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Reset the form by changing the content type field to null when clicking on it.
   */
  Drupal.behaviors.contentTypeReset = {
    attach: function (context, settings) {
      let lastInput = $('select[name="available_field_types"]');
      $('select[name="content_type"]').click(function () {
        if (lastInput.length != 0) {
          if ($(this).val().length != 0 && lastInput.val().length != 0) {
            $('option[selected="selected"]').prop('selected', true);
            $(this).trigger('change');
          }
        }
      })
    }
  };

  /**
   * Machine name suggestions for label field.
   */
  Drupal.behaviors.autoMachineName = {
    attach: function (context, settings) {
      let machineNameInput = $('.form-item-field-machine-name');
      let machineNameText = $('div[data-drupal-selector="edit-auto-machine-name"]');
      machineNameText.hide();
      $('#edit-field-machine-name').once().before($('<span>field_</span>'));

      // Machine name js auto-format
      if ($('input.error').length == 0) {
        machineNameInput.hide();

        let modif = false;

        let trimmer = {
          replace_pattern: "[^a-z0-9_]+",
          maxlength: "26",
          replace: "_",
        }

        let rx = new RegExp(trimmer.replace_pattern, 'g');

        $(document).keyup(function () {
          if (!modif) {
            let text = $('#edit-field-label').val();
            if (text.length > 0) {
              machineNameText.show();
            } else {
              machineNameText.hide();
            }
            let expected = text.toLowerCase().replace(rx, trimmer.replace).substr(0, trimmer.maxlength);
            $('#amnVal').text(expected)
            $('#edit-field-machine-name').val(expected)
          }
        })

        $('#edit-new-field-config span.link').click(function () {
          modif = true;
          machineNameText.hide();
          machineNameInput.show();
        })
      }
    }
  };

  /**
   * Hiding number field for cardinality if unlimited.
   */
  Drupal.behaviors.hideCardinalityNumber = {
    attach: function (context, settings) {
      let cardInput = $('#edit-new-field-cardinality');
      $('.form-item-new-field-cardinality-limited').append(cardInput)
      $('#edit-new-field-cardinality-limited').change(function () {
        if ($(this).val() == 'unlimited') {
          cardInput.hide();
        } else {
          cardInput.show();
        }
      })
    }
  };

})(jQuery, Drupal, drupalSettings);
